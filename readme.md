# Zeiterfassung und Erfassung der User Stories im BP 

Zur einfacheren Zeiterfassung und der Bestimmung der Velocity, stellen wir diverse Vorlagen (als .csv) zur Verfügung. Es dürfen jederzeit andere Tools zur Zeiterfassung und zur Dokumentation der User Stories verwendet werden, es müssen aber trotzdem alle Informationen aus den Vorlagen enthalten sein.

* Der Ordner `user_stories` beinhaltet eine Datei für jede User Story im Projekt. Dazu kann jeweils die Vorlage kopiert und ausgefüllt werden.
* Der Ordner `personen` beinhaltet eine Datei pro Person für das individuelle Zeittracking. Auch dazu kann jeweils die Vorlage kopiert und angepasst werden.

Zum Erstellen einer Zusammenfassung inkl. der Velocity einer Iteration kann `python3 generate_summary.py` ausgeführt werden.
**Hinweis**: Die Velocity wird nur über alle abgenommenen User Stories der gesamten Iteration berechnet! Bei noch nicht erfassten Iterationen wird ein '-' eingetragen. Eine Velocity von 0 besagt, dass im Ordner `user_stories` keine User Story hinterlegt ist, die der entsprechenden Iteration entspricht.

Zum einfachen Bearbeiten können die .csv Dateien in Excel oder LibreOffics Calc geöffnet werden. Der export sollte dann mit Trennzeichen ';' und Maskierzeichen '"' erfolgen.

**Hinweis:** Wir verwenden die Kalenderwochen nach ISO Standard, d.h. die Woche beginnend mit dem 28.12. ist KW 53, nicht KW 1. US-amerikanische Systeme können da ggf. abweichen...

