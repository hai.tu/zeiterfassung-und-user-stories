"""
methods for reading user story and time tracking csv files in the BP
"""

import csv
import os


def read_userstories(directory):
    """
    read user story csv files

    :param directory: directory containing user story files
    :return: Dict containing one entry per user story
    """
    data = {}

    # read every file in the specified directory
    for in_file in os.listdir(directory):
        with open(os.path.join(directory, in_file)) as csv_file:
            us_data = {}
            reader = csv.reader(csv_file, delimiter=';', quotechar='"')
            for row in reader:
                row_id_lower = row[0].lower()
                if 'id' in row_id_lower:
                    us_data['id'] = row[1]
                elif 'iteration' in row_id_lower:
                    us_data['iteration'] = int(row[1])
                elif 'entwickler*in' in row_id_lower:
                    us_data['developer'] = row[1]
                elif 'tatsächlich' in row_id_lower:
                    us_data['hours'] = float(row[1].replace(',', '.'))
                elif 'geschätzt' in row_id_lower:
                    us_data['story-points'] = int(row[1])
            data[in_file[:-4]] = us_data # use file name without '.csv' as key
    return data


def read_time_entries(directory):
    """
    read individual time tracking entries (from csv files)
    
    :param directory: directory containing time tracking files
    :return: Dict with one entry per developer
    """
    data = {}

    for in_file in os.listdir(directory):
        with open(os.path.join(directory, in_file)) as csv_file:
            time_data = {}
            name = ""
            reader = csv.reader(csv_file, delimiter=';', quotechar='"')
            for row in reader:

                # skip empty rows
                if not row: continue

                # read row containing developer's name
                if 'name' in row[0].lower():
                    name = row[1]
                    continue
                # ignore row specifying column names
                if 'kw' in row[0].lower():
                    continue

                # read weekly entries
                kw, iteration, us, orga, vortrag, qs = row
                time_data[kw] = {
                    'iteration': iteration,
                    'us': us,
                    'orga': orga,
                    'vortrag': vortrag,
                    'qs': qs
                }
            data[name] = time_data

    return data
