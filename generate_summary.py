#!/usr/bin/env python3
""" Simple pythonscript to update indivual user tables with user_stories
"""

import csv

from bp_reader import read_userstories, read_time_entries


def compute_velocity(stories, iteration):
    storypoints = [v['story-points'] for k, v in stories.items()
                   if str(iteration) == str(v['iteration'])]
    hours = [v['hours'] for k, v in stories.items()
             if str(iteration) == str(v['iteration'])]
    if sum(hours) == 0:
        return 0
    return float(sum(storypoints)) / float(sum(hours))


def sum_hours(developers, iteration):
    names = sorted(list(developers.keys()))
    hours = []
    for name in names:
        dev_hours = [v for k, v in developers[name][str(iteration)].items()]
        hours.append(sum([float(v.replace(',', '.')) for v in dev_hours if v != '']))
    return hours


# if this script is called directly
if __name__ == '__main__':

    user_stories = read_userstories('user_stories') # TODO: bei abweichenden Pfaden anpassen
    developers = read_time_entries('personen') # TODO: bei abweichenden Pfaden anpassen

    kws = ['46', '47', '48', '49', '50', '51', '52', '53',
           '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13']

    # write summary csv file
    with open("summary.csv", 'w') as out_file:
        writer = csv.writer(out_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        names = sorted(list(developers.keys()))

        # write column labels
        writer.writerow(['KW', 'Iteration'] + names + ['Gesamt', 'Velocity'])

        # write rows for every KW
        for i, kw in enumerate(kws):
            current_iteration = developers[names[0]][kw]['iteration']
            if current_iteration == '':  # ignore not tracked iterations/iterations need to be provided in every file
                print("No complete iteration in KW: {}".format(kw))
                writer.writerow([kw, '', '', '', '-'])
                continue
            velocity = '-'
            if i == len(kws) - 1 or current_iteration != developers[names[0]][kws[i + 1]]['iteration']:
                velocity = round(compute_velocity(user_stories, current_iteration), 2)
            hours = sum_hours(developers, kw)
            writer.writerow([kw, current_iteration]
                            + hours
                            + [sum(hours), str(velocity)])
